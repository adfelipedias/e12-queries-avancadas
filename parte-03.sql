SELECT * FROM enderecos;

SELECT * FROM usuarios as u
INNER JOIN enderecos as e
ON u.endereco_id = e.id;

SELECT * FROM usuario_redes_sociais as urs 
JOIN usuarios as u
ON urs.usuario_id = u.id
JOIN redes_sociais as rs
ON urs.rede_social_id = rs.id;

SELECT * FROM usuario_redes_sociais as urs
JOIN usuarios as u
ON urs.usuario_id = u.id
JOIN redes_sociais as rs
ON urs.rede_social_id = rs.id
JOIN enderecos as e 
ON u.endereco_id = e.id;

SELECT rs.nome as redes_social, u.nome as usuario, u.email, e.cidade 
FROM usuario_redes_sociais as urs
JOIN usuarios as u
ON urs.usuario_id = u.id
JOIN redes_sociais as rs
ON urs.rede_social_id = rs.id
JOIN enderecos as e 
ON u.endereco_id = e.id;

SELECT rs.nome as redes_social, u.nome as usuario, u.email, e.cidade 
FROM usuario_redes_sociais as urs
JOIN usuarios as u
ON urs.usuario_id = u.id
JOIN redes_sociais as rs
ON urs.rede_social_id = rs.id
JOIN enderecos as e 
ON u.endereco_id = e.id
WHERE rs.nome LIKE 'Youtube';

SELECT rs.nome as redes_social, u.nome as usuario, u.email, e.cidade 
FROM usuario_redes_sociais as urs
JOIN usuarios as u
ON urs.usuario_id = u.id
JOIN redes_sociais as rs
ON urs.rede_social_id = rs.id
JOIN enderecos as e 
ON u.endereco_id = e.id
WHERE rs.nome LIKE 'Instagram';

SELECT rs.nome as redes_social, u.nome as usuario, u.email, e.cidade 
FROM usuario_redes_sociais as urs
JOIN usuarios as u
ON urs.usuario_id = u.id
JOIN redes_sociais as rs
ON urs.rede_social_id = rs.id
JOIN enderecos as e 
ON u.endereco_id = e.id
WHERE rs.nome LIKE 'Facebook';

SELECT rs.nome as redes_social, u.nome as usuario, u.email, e.cidade 
FROM usuario_redes_sociais as urs
JOIN usuarios as u
ON urs.usuario_id = u.id
JOIN redes_sociais as rs
ON urs.rede_social_id = rs.id
JOIN enderecos as e 
ON u.endereco_id = e.id
WHERE rs.nome LIKE 'TikTok';

SELECT rs.nome as redes_social, u.nome as usuario, u.email, e.cidade 
FROM usuario_redes_sociais as urs
JOIN usuarios as u
ON urs.usuario_id = u.id
JOIN redes_sociais as rs
ON urs.rede_social_id = rs.id
JOIN enderecos as e 
ON u.endereco_id = e.id
WHERE rs.nome LIKE 'Twitter';
