DROP TABLE IF EXISTS enderecos, usuarios, redes_sociais, usuario_redes_sociais;

CREATE TABLE IF NOT EXISTS enderecos (
    id              bigserial           PRIMARY KEY,
    rua             varchar             NOT NULL,
    pais            varchar(100)        NOT NULL,
    cidade          varchar(100)        NOT NULL
);

CREATE TABLE IF NOT EXISTS usuarios (
    id                  bigserial           PRIMARY KEY,
    nome                varchar(100),
    email               varchar             NOT NULL UNIQUE,
    senha               varchar             NOT NULL,
    endereco_id         integer             NOT NULL REFERENCES enderecos(id)
);

CREATE TABLE IF NOT EXISTS redes_sociais (
    id                   bigserial           PRIMARY KEY,
    nome                 varchar(150)         NOT NULL UNIQUE       
);

CREATE TABLE IF NOT EXISTS usuario_redes_sociais (
    id                           bigserial           PRIMARY KEY,
    usuario_id                   integer             NOT NULL REFERENCES usuarios(id),
    rede_social_id               integer             NOT NULL REFERENCES redes_sociais(id)
);

